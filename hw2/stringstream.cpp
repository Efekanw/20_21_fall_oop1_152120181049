#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
    vector<int> num;
    char c;
    int x;
    stringstream ss(str);  /// In order to parsing integers
    while (ss)              /// Repeats until the string ends
    {
        ss >> x >> c;           /// Parsing integer to comma
        num.push_back(x);   /// Integer value is assigned to vector num with push_back function
    }
    return num;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    return 0;
}