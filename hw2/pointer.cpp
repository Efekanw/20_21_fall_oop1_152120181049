#include <stdio.h>

void update(int* a, int* b) {
    int sum;
    int diff;
    sum = *a + *b;
    diff = *a - *b;
    if (diff < 0)  // diff value is provided to be positive
    {
        diff *= -1;
    }
    *a = sum;     //sum value is assing to a variable
    *b = diff;    //diff value is assing to a variable
}

int main() {
    int a, b;
    int* pa = &a, * pb = &b;

    scanf_s("%d %d", &a, &b); // Values are get from user
    update(pa, pb);           // update function is called
    printf("%d\n%d", a, b);   

    return 0;
}
