#include <iostream>
#include <string>
using namespace std;

int main() {
    string a, a_new;
    string b, b_new;
    cin >> a;
    cin >> b;
    cout << a.size() << " ";           /// string a's length is printed
    cout << b.size() << endl;          /// string b's length is printed
    cout << a + b << endl;               /// Product of strings is printed
    a_new = a;
    a_new[0] = b[0];               /// string b's first index value is assinged to sting a_new's first index
    b_new = b;
    b_new[0] = a[0];              /// string a's first index value is assinged to sting b_new's first index
    cout << a_new << " ";
    cout << b_new;

    return 0;
}