#include <iostream>
#include <cstdio>
#include <iomanip>   // In order to use setpresion and fixed functions
using namespace std;

int main() {
    // Complete the code.
    int i;
    long l;     // Variables are declared
    char c;
    float f;
    double d;
    cin >> i >> l >> c >> f >> d;  // Values of variables are get from user
    cout << i << endl << l << endl << c << endl;  // Values are printed
    cout << fixed << setprecision(3) << f<<endl;  // used for 3 decimal 
    cout << fixed << setprecision(9) << d;  // used for 9 decimal
    return 0;
}