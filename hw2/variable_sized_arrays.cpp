#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int n, q;
    int k, i, j, number;
    cin >> n >> q;
    vector<vector<int>> a(n);           /// provides dynamic memory. n number of vectors created
    for (int t = 0; t < n; t++)
    {
        cin >> k;                       /// k value is get from user
        for (int y = 0; y < k; y++)     
        {
            cin >> number;              /// Numbers to be assinged into the vector are taken from user.Then numbers are assigned into vector's 't'. index
            a[t].push_back(number);     
        }

    }
    for (int t = 0; t < q; t++)
    {
        cin >> i >> j;                  /// User enters the index values of the number which wants to get
        cout << a[i][j] << endl;        /// That number is printed to the screen
    }
    return 0;

}