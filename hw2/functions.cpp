#include <iostream>
#include <stdio.h>
#include <cstdio>
using namespace std;

int max_of_four(int a, int b, int c, int d)
{
    int max = 0; // max variable is declared
    max = a;     // The value of the first number is assigned to the variable max
    if (b > max)  // if b is greater than maximum value, new maximum becomes b
    {
        max = b;
    }
    if (c > max)   // if c is greater than maximum value, new maximum becomes b
    {
        max = c;
    }
    if (d > max)   // if d is greater than maximum value, new maximum becomes b
    {
        max = d;
    }
    return max; // max is returned

}
int main() {
    int a, b, c, d;
    scanf_s("%d %d %d %d", &a, &b, &c, &d); // Values of variables are get from user
    int ans = max_of_four(a, b, c, d);    // funciton is called and return value is equaled to ans
    printf("%d", ans);

    return 0;
}