#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

/*
    add code for struct here.
*/
struct Student {         /// Student struct is defined
    int age;            /// Declared age
    string first_name;  /// Declared first_name
    string last_name;   /// Declared last_name
    int standard;       /// Declared standard
};
int main() {
    Student st;

    cin >> st.age >> st.first_name >> st.last_name >> st.standard;
    cout << st.age << " " << st.first_name << " " << st.last_name << " " << st.standard;

    return 0;
}