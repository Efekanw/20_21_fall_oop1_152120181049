#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    int a, b;
    cin >> a;
    cin >> b;
    for (int n = a; n <= b; n++)
    {
        if (n <= 9 && n >= 1)
        {
            if (n == 1)           // n variable is checked 
            {
                cout << "one" << endl;
            }
            else if (n == 2)    // n variable is checked 
            {
                cout << "two" << endl;
            }
            else if (n == 3)    // n variable is checked 
            {
                cout << "three" << endl;
            }
            else if (n == 4)    // n variable is checked 
            {
                cout << "four" << endl;
            }
            else if (n == 5)    // n variable is checked 
            {
                cout << "five" << endl;
            }
            else if (n == 6)    // n variable is checked 
            {
                cout << "six" << endl;
            }
            else if (n == 7)    // n variable is checked 
            {
                cout << "seven" << endl;
            }
            else if (n == 8)    // n variable is checked 
            {
                cout << "eight" << endl;
            }
            else if (n == 9)    // n variable is checked 
            {
                cout << "nine" << endl;
            }
        }
        else if (n > 9)
        {
            if (n % 2 == 0)          // Is n even or odd is checked 
                cout << "even" << endl;
            if (n % 2 == 1)
                cout << "odd" << endl;
        }

    }
    return 0;
}