#include<iostream>
#include<stdlib.h>
#include<fstream>
#define MAX 100
using namespace std;
int sumof(int num[MAX],int size) /// sumof function is defined
{
	int sum = 0;
	for (int m = 0; m < size; m++)	
	{
		sum += num[m];          /// values which in num array are summed and equaled to sum variable
	}
	return sum;
}
int productof(int num[MAX], int size) /// productof function is defined
{
	int product = 1;
	for (int m = 0; m < size; m++)
	{
		product *= num[m];		/// values which in num array are producted and equaled to product variable
	}
	return product;
}
int smallestof(int num[MAX], int size) /// smallestof function is defined
{
	int smallest = num[0];            /// first value in array is chosen by smallest
	for (int m = 1; m < size; m++)
	{
		if (num[m] < smallest)		  /// next number is checked in if condition.
		{
			smallest = num[m];		  ///If is smallest from smallest variable, smallest variable becomes next number
		}
	}
	return smallest;

}
int main() {
	ifstream file;
	int N[MAX];
	int size=0;
	int i = 0;
	int j = 0;
	float avarage = 0;
	file.open("input.txt");
	if (!file)										/// check whether the file has been opened or not
	{
		cout << "Could not be opened";
		exit(1);
	}

	do
	{
		if (i == 0)
		{
			file >> size;						/// How many numbers in the file are kept in the variable named size
			if (size <= 0)						/// size value can not be less than 0
			{
				cout << "Error!! size is entered incorrectly ";
				exit(0);
			}
			i++;
		}
		else
		{
			file >> N[j];						/// the numbers in the file are stored in the N array
			j++;
		}
		
		
	} while (!file.eof());

	if (size == j ) {												/// size value is checked
		avarage = sumof(N, size) / (float)size;						/// avarage is calculated

		cout << "Sum is " << sumof(N, size) << endl;				/// sumof function is called and return value is printed
		cout << "Product is " << productof(N, size) << endl;		/// productof function is called and return value is printed
		cout << "Avarage is " << avarage << endl;					/// avarage is printed
		cout << "Smallest is " << smallestof(N, size) << endl;		/// smallestof function is called and return value is printed
	}
	else
	{
		cout << "Error!! size is entered incorrectly ";				
		exit(0);
	}
			
}
